# https://python-packaging.readthedocs.io
from setuptools import setup

setup(
    name="hawc2_gbar_launcher",
    version="0.1",
    description="A CLI tool launch HAWC2 simulations",
    # url                  =
    author="Jenni Rinker",
    author_email="rink@dtu.dk",
    packages=["hawc2_gbar_launcher"],
    install_requires=["click", "jinja2", "tqdm"],
    entry_points={
        "console_scripts": [
            "hawc2launch=hawc2_gbar_launcher.hawc2_gbar_launcher:launch"
        ]
    },
)
