import click
from pathlib import Path
from .backend import make_filelist, render_jobscript, LOG_DIR
import os

RUN_COMMAND = "bsub < run.sh"
FILELIST_FN = "to_run.txt"


@click.command()
# arguments
@click.argument("htc_dir", type=click.Path(exists=True))
# optional flags (include flag to enable)
@click.option(
    "--skip-complete",
    is_flag=True,
    help="Skip simulations which have already successfully run",
)
@click.option("--email_start", is_flag=True, help="Receive email on job start")
@click.option("--email_stop", is_flag=True, help="Receive email on job end")
@click.option("-R", is_flag=True, help="Search for htc files recursively")
# optional arguments
@click.option("-m", "--maxcores", default=10, help="Max number of cores to use (default: 10)")
@click.option("-n", "--nodes", default=1, help="Number of cores per simulation (default: 1)")
@click.option("-q", "--queue", default="hpc", help="Queue to submit jobs to (default: hpc)")
@click.option("-w", "--walltime", default="00:30", help="Maximum walltime of a simulation (default: 30 mins)")


def launch(htc_dir, maxcores, nodes, skip_complete, r,
           queue, walltime, email_start, email_stop):
    """Prep job script and (optionally) launch job script.
    """
    print('Getting list of htc files to run...')
    n_sims = make_filelist(
        htc_dir, FILELIST_FN, recursive=r, skip_complete=skip_complete
    )

    print('Making job script...')
    render_jobscript(FILELIST_FN,
                     maxcores=maxcores, nodes=nodes, queue=queue, walltime=walltime,
                     email_start=email_start, email_stop=email_stop)

    print('Creating directory for LSF log files...\n')
    Path(LOG_DIR).mkdir(parents=True, exist_ok=True)

    confirmation_message = (
        f"{n_sims} simulations on {maxcores} node(s) ready to launch on {queue}. Continue?"
    )

    if click.confirm(confirmation_message):
        os.system(RUN_COMMAND)
