import os
import jinja2
from pathlib import Path
from tqdm import tqdm
import re

SCRIPT_TEMPLATE_FN = Path(__file__).parent / "run.sh.j2"
LOG_DIR = "lsf_log"

# Regular expression to find the first two words in an htc file.
exp_line = re.compile(r"^[ \t]*([a-zA-Z]+)[ \t]+([\w\.\\/]+).*")


def extract_res_filename(htc_file_name):
    """Extract the results file name from a htc file.

    Parameters
    ----------
    htc_file_name : str
        Path of the htc file.

    Returns
    -------
    res_file_name : str
        The results file.
    """
    with open(htc_file_name, "r") as fid:
        htc_file_txt = fid.readlines()

    # Type of a first level block.
    block_type = ""
    # True if the current line is inside a first level block.
    in_block = False
    res_file_name = ""

    # Look for a first level output block.
    for i_line, line in enumerate(htc_file_txt):
        # Split the line into the first two words.
        elements = re.findall(exp_line, line)
        if len(elements) == 0:
            continue
        if (not in_block) and (elements[0][0] == "begin"):
            # Open a first level block.
            block_type = elements[0][1]
            in_block = True
            if block_type == "output":
                break
        elif (elements[0][0] == "end") and (elements[0][1] == block_type):

            in_block = False

    # Look for a filename command.
    for i in range(i_line, len(htc_file_txt)):
        # Split the line.
        elements = re.findall(exp_line, htc_file_txt[i])
        if len(elements) == 0:
            continue
        if elements[0][0] == "filename":
            res_file_name = elements[0][1]
            break

    return Path(res_file_name)


def successfully_run(htc_fn):
    """Extracts the result file name from an htc file and checks if it exists.
    """
    res_fn = extract_res_filename(htc_fn).as_posix().lower()
    if any(Path(res_fn + ext).exists() for ext in [".sel", ".dat", ".hdf5"]):
        return True
    else:
        return False


def make_filelist(htc_dir, fn_out, recursive=False, skip_complete=False):
    """Create a list of htc files in a directory."""
    htc_dir = Path(htc_dir)
    assert htc_dir.exists(), f"{htc_dir} does not exist."
    if htc_dir.is_dir():

        if recursive:
            files_all = list(htc_dir.rglob("*.htc"))
        else:
            files_all = list(htc_dir.glob("*.htc"))
    else:
        files_all = [htc_dir]

    if skip_complete:
        files = []
        for fn in tqdm(files_all, desc="Finding incomplete simulations"):
            if not successfully_run(fn):
                files.append(fn)

    else:
        files = files_all

    N_sims = len(files)

    with open(fn_out, "w") as f:
        for fn in files:
            f.write(fn.as_posix() + "\n")

    return N_sims


def render_jobscript(
    fn_list,
    maxcores=10,  # max no. cores to use
    nodes=1,  # cores per h2 simulation
    queue="hpc",
    walltime="00:30",  # walltime per sim (30 min default)
    email_start=True,  # get email at sim start?
    email_stop=True,  # get email at sim end?
    script_fn="run.sh",
    jobname="hawc2"
):
    """Create a job script from the jinja template."""

    n_sims = sum(1 for line in open(fn_list))
    try:
        email = f"{os.environ['USER']}@dtu.dk"
    except KeyError:
        email = f"{os.environ['USERNAME']}@dtu.dk"
    max_jobs = maxcores // nodes  # max simultaneous jobs
    email_start = ['#', ''][email_start]  # convert to comment if no
    email_stop = ['#', ''][email_stop]  # convert to comment if no

    # keys to replace in jinja template
    context = {
        "jobname": jobname,
        "n_sims": n_sims,
        "max_jobs": max_jobs,
        "queue": queue,
        "nodes": nodes,
        "walltime": walltime,
        "email": email,
        "email_start": email_start,
        "email_stop": email_stop,
        "htc_list_fn": fn_list,
        "LOG_DIR": LOG_DIR,
    }

    with open(SCRIPT_TEMPLATE_FN) as f:
        template = jinja2.Template(f.read())

    out = template.render(context)

    with open(script_fn, "w") as f:
        f.write(out)

    return n_sims
