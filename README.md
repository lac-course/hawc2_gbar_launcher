# HAWC2 GBar Launcher

This Python package can be used to launch a collection of HAWC2 input files on the GBar cluster. The program has three steps:  
1. Generate list of htc files to simulate  
2. Create job script and job log folder and  
3. (Optional) Launch the job script.  

You can cancel the launching of the job script and instead modify the script before launching it manually, should you so desire.

The htc files to run should be generated locally on your computer using a method of your choice and then transferred to the cluster.

A big thank-you to Jaime Liew for developing the HAWC2 Sophia Launcher, from which much of this codebase is taken.

## 1. Quick guide: launching your simulations

**NOTE** These steps assume you already set up the model files and HAWC2 GBar Launcher (see section below).

1. Load the Python module:  
```
module load python3/3.9.11
``` 
2. Add the launcher folder to your path:  
```
export PATH="~/.local/bin:$PATH"
```
3. Change to your model directory. For example:  
```
cd ~/hawc2/dtu_10mw/
```
4. Launch your simulations with your desired settings.
   For example, here is the command for launching all htc files in all subdirectories of folder `htc/` using a maximum of 4 cores total:  
```
hawc2launch -R -m 4 htc
```
 

If you want to change your job script and/or submit it manually, enter "n" in the hawc2 launcher before launching the simulations, and then manually submit your job script:  
```
bsub < run.sh
```
5. When your simulations are done, post-process them and/or transfer them off the cluster, then delete your model folder.

## 2. Set up your folders

Before you can launch your simulations, you need to set up your model and the GBar Launcher.

### 2.1 Set up your model directory

1. Create your model folder, e.g., `~/hawc2/dtu_10mw/`.  
2. Into this model folder, transfer in your `htc/` directory and any other folders you need to run your model.
   For example, this could be your `control/` and `data/` folders.

**Note** that to run a HAWC2 model on linux, you need linux combiled binaries instead of DLLs! In other words, any files that end in `.dll` in a Windows model, you need to get the corresponding `.so` file for Linux. You can **not** simply rename your `.dll` files to `.so`, they are compiled differently. You can find some Linux files in [this shared folder](https://files.dtu.dk/u/3KWw9X-lsOHOpbD2/95cbba48-bc3d-435e-b687-dcccbea013da?l). For other files, or newer versions, please have your supervisor reach out to the HAWC2 development team.


### 2.2 Configure the HAWC2 Gbar launcher

1. Load the Python module, if you have not done so already (see above).  
2. Clone the HAWC2 gbar launcher, we suggest into your home directory:  
```
cd ~
git clone https://gitlab.windenergy.dtu.dk/lac-course/hawc2_gbar_launcher.git
```
3. Change directory into the launcher
```
cd hawc2_gbar_launcher
```
4. Load the Python module:  
```
module load python3/3.9.11
```
5. Install the launcher in an editable form (in case you want to change something):  
```
pip install -e .
```

## How to learn about GBar

The GBar cluser docs are here: https://www.hpc.dtu.dk/?page_id=2910. The User Guides in particular can explain the parameters that are used in the job script.


## Complete list of options

Use `hawc2launch --help` to display these options on the command line.

|            Option             | Description                                                                |
| :---------------------------: | -------------------------------------------------------------------------- |
|       `--skip-complete`       | Skip simulations which have already successfully run                       |
|             `-R `             | Search for htc files recursively                                           |
|       `--email-start `        | Receive an email when job starts                                           |
|       `--email-stop `         | Receive an email when job stops                                            |
|     `-q`, `--queue` TEXT      | Queue to submit jobs to (default: `hpc`)                                   |
|    `-n`, `--nodes` INTEGER    | Number of cores per simulation (default: `1`)                              |
|   `-m`, `--maxcores` INTEGER  | Maximum number of cores to use (default: `10`)                             |
|       `--walltime` TEXT       | Maximum walltime of simulations (default: 30 minutes)                      |
|           `--help`            | Show this message and exit.                                                |

